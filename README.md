# README #

Splunk custom view example to illustrate Hex View in Splunk dashboard table.

### Screenshot ###

![Screen Shot](appserver/screenshot.png)

### How to install? ###

* cd $SPLUNK_HOME/etc/apps
* git clone -b master https://<your_bitbucket_id>@bitbucket.org/splunkkorea/alex_hex_viewer.git
* When you install this app not by git clone but using download menu, be sure to **rename the app folder to original** (alex_hex_viewer)