require.config({
    paths: {
        hexy: "../app/alex_hex_viewer/hexy",
        hexview_simple: "../app/alex_hex_viewer/hexview"
    }
});
require([
    'splunkjs/mvc',
    'splunkjs/mvc/searchmanager',
    'underscore',
    'hexy',
    'hexview_simple',
    'splunkjs/mvc/simplexml/ready!'
], function(
    mvc,
    SearchManager,
    _,
    hexy,
    HexView
) {
    var getParameterByName = function(name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results == null ? "No Data" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }

    var _searchManager = new SearchManager({
        id: "hexview-details-search-manager",
        preview: true,
        cache: true,
        search: 'index="hex" sourcetype="ids" hexdumpid="' + getParameterByName("hexdumpid") + '" | table hexdump'
    });

    var customView = new HexView({
        id: "simpleHexView2",
        managerid: "hexview-details-search-manager",
        el: $("#simpleHexView2")
    }).render();
});
