/* 
hexview.js
Splunk custom view component to render hex encoded binary data into traditional Hex Viewer form.
*/

define(function(require, exports, module) {
    // Base class for custom views
    var SimpleSplunkView = require('splunkjs/mvc/simplesplunkview');
    require('./hexy');

    // Define the custom view class
    var HexView = SimpleSplunkView.extend({
        className: "hexview",

        // Define our initial values, set the type of results to return
        options: {
            format: "twos", // ["fours"|"twos"|"none"], how many nibbles per group
            prefix: "0x" // something pretty to put in front of each line
        },

        createView: function() {
            return this;
        },

        updateView: function(viz, data) {
            // var myResults = data[0]; // Sets this to the first (and only) row
            var result = data[0][0];

            // Convert back hex converted data(data from search) to binary string
            var _hex2bin = function(hex) {
                var bytes = [],
                    str;
                for (var i = 0; i < hex.length - 1; i += 2)
                    bytes.push(parseInt(hex.substr(i, 2), 16));

                return String.fromCharCode.apply(String, bytes);
            }

            // Hexy config
            var config = { format: this.options.format, prefix: this.options.prefix };
            var bin = _hex2bin(result);
            var hex = new Hexy(bin, config);

            // $td.addClass("icon").html(_.template('<pre><%-alex%></pre>', {
            //     alex: result
            // }));
            this.$el.html('<pre>' + hex.toString() + '</pre>');

            return this;
        }
    });
    return HexView;
});
