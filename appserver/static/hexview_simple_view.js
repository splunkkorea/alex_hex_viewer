require.config({
    paths: {
        hexy: "../app/alex_hex_viewer/hexy",
        hexview_simple: "../app/alex_hex_viewer/hexview_simple"
    }
});
require([
    'splunkjs/mvc',
    'underscore',
    'hexy',
    'hexview_simple',
    'splunkjs/mvc/simplexml/ready!'
], function(
    mvc,
    _,
    hexy,
    HexView
) {
    var getParameterByName = function(name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results == null ? "No Data" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }

    var customView = new HexView({
        id: "simpleHexView",
        el: $("#simpleHexView")
    }).render(getParameterByName("hexValue"));
});
