require.config({
    paths: {
        hexy: "../app/alex_hex_viewer/hexy",
        hexview: "../app/alex_hex_viewer/hexview"
    }
});
require([
    'splunkjs/mvc/tableview',
    'splunkjs/mvc/searchmanager',
    'splunkjs/mvc',
    'underscore',
    'hexy',
    'hexview',
    'splunkjs/mvc/simplexml/ready!'
], function(
    TableView,
    SearchManager,
    mvc,
    _,
    hexy,
    HexView
) {
    var EventSearchBasedRowExpansionRenderer = TableView.BaseRowExpansionRenderer.extend({

        initialize: function(args) {
            // initialize will run once, so we will set up a search and a chart to be reused.
            this._searchManager = new SearchManager({
                id: 'details-search-manager',
                preview: false
            });
            this._hexView = new HexView({
                managerid: 'details-search-manager'
            });
        },

        getInitialDataParams: function() {
            return ({
                outputMode: SplunkVisualizationBase.RAW_OUTPUT_MODE
            });
        },

        canRender: function(rowData) {
            return true;
        },

        render: function($container, rowData) {
            // rowData contains information about the row that is expanded.  We can see the cells, fields, and values
            // We will find the sourcetype cell to use its value
            var hexdumpCell = _(rowData.cells).find(function(cell) {
                return (cell.field === 'hexdumpid');
            });

            //update the search with the sourcetype that we are interested in
            this._searchManager.set({ search: 'index="hex" sourcetype="ids" hexdumpid="' + hexdumpCell.value + '" | table hexdump' });

            $container.append(this._hexView.createView().el);
        }
    });

    var tableElement = mvc.Components.getInstance("hexview_expandable_table");

    tableElement.getVisualization(function(tableView) {
        // Add custom cell renderer, the table will re-render automatically.
        tableView.addRowExpansionRenderer(new EventSearchBasedRowExpansionRenderer());
    });
});
